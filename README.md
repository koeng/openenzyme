# The OpenEnzyme Project / Consortium

The affordable, reliable, and simple assembly of DNA is essential to the growth of synthetic biology in the 21st century. Unfortunately, the power of this technology isn't accessible to everyone, especially those in locations whose access to enzymes is limited by customs and logistics of cold chains, or those who live where incumbent companies refuse to collaborate.

Our goal is to create an open system for producing a scalable quantity of enzymes anywhere in the world. In particular, we are focusing on enzymes related to DNA assembly technology such as MoClo or Gibson. 

